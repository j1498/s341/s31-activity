//Controllers contain the functions and business logic of our Express JS app
const Task = require("../models/task");

//Controller function for getting all the tasks
//Defines the functions to be used in the "taskRoute.js" file and exports these functions

module.exports.getAllTasks = () => {

	//model.method
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {

	let newTask = new Task ({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			return task
		}
	})
}


//Controller function for deleting a task
module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if(err){
			console.log(err)
			return false
		} else {
			return removedTask
		}

	})

}

//Controller function for updating a task
module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) =>{

			if (saveErr){
				console.log(saveErr)
				return false
			} else {
				return updatedTask
			}
		})
	})

}



// Activity

// Controller to get a specific task


module.exports.getSpecificTask = (taskId) => {
	return Task.findOne({_id: taskId}).then(searchedTask => searchedTask)
}


// Controller to update a specific task

module.exports.updateStatus = (TaskId) => {
	return Task.findById(TaskId).then((searchedTask, error) => {
		if (error) {
			console.log(error);
			return false;
		}

		searchedTask.status = 'complete'

		return searchedTask.save().then((updateResult, updateError) => {
			if (updateError) {
				console.log(updateError)
				return false
			} else {
				return updateResult
			}
		})
	})
}
